﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using CommandLine;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;

namespace xls2text
{
    class Program
    {
        public class CmdOptions
        {
            [Option ('f', "file", Required = true, HelpText = "Set input file path.")]
            public string File { get; set; }

            [Option ('o', "output", Default = "test.docx", Required = false, HelpText = "Set file path.")]
            public string Output { get; set; }

            [Option ('m', "table-mode", Default = 1, Required = false, HelpText = "integration table(1), unit test table(2)")]
            public int TableMode { get; set; }
        }

        public static void Main (string[] args)
        {
            CommandLine.Parser.Default
                .ParseArguments<CmdOptions> (args)
                .WithParsed<CmdOptions> (Run);
        }

        static void Run (CmdOptions options)
        {
            if (!File.Exists (options.File))
                throw new FileNotFoundException ($"File not found: {options.File}");
            else
            {
                StartProcess (options.File, options.Output, options.TableMode);
                return;
            }
        }

        static void StartProcess (string file, string output, int mode)
        {
            string fileName = System.IO.Path.Combine (Environment.CurrentDirectory, output);
            using (var fileStream = File.Open (fileName, FileMode.Create))
            {
                var wr = OpenXmlWriter.Create (fileStream, encoding : System.Text.Encoding.GetEncoding ("utf-16"));
                var document = WordprocessingDocument.Create (fileStream, WordprocessingDocumentType.Document);
                document.AddMainDocumentPart ();
                document.MainDocumentPart.Document = new Document (new Body ());

                switch (mode)
                {
                    case 1:
                        {
                            var t = new T1.Table1 ();
                            t.ParseCSV (file, (model) =>
                            {
                                // Add new line
                                document.MainDocumentPart.Document.Body.Append (new Paragraph ());

                                var table = T1.Table1.GetTable (model);
                                if (table != null)
                                {
                                    document.MainDocumentPart.Document.Body.AppendChild (table);
                                    document.MainDocumentPart.Document.Body.Append (new Paragraph ());
                                }

                                // var pageBreak = new Paragraph(new Run(new Break() { Type = BreakValues.Page }));
                                // document.MainDocumentPart.Document.Body.Append(pageBreak);
                            });
                            break;
                        }
                    case 2:
                        {
                            var t = new T2.Table2 ();
                            t.ParseCSV (file, (model) =>
                            {
                                // Add new line
                                document.MainDocumentPart.Document.Body.Append (new Paragraph ());

                                var table = T2.Table2.GetTable (model);
                                if (table != null)
                                {
                                    document.MainDocumentPart.Document.Body.AppendChild (table);
                                    document.MainDocumentPart.Document.Body.Append (new Paragraph ());
                                }

                                // var pageBreak = new Paragraph(new Run(new Break() { Type = BreakValues.Page }));
                                // document.MainDocumentPart.Document.Body.Append(pageBreak);
                            });
                            break;
                        }
                }

                document.Dispose ();
            }
        }
    }
}