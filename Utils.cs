using System;
using System.Text.RegularExpressions;

namespace xls2text
{
    public class Utils
    {
        public static string[] ParseCsvLine (string line, int requiredSize)
        {
            // Clean new line
            line = line.Replace (Environment.NewLine, "");

            // Split with ,
            var vals = Regex.Split (line, ",(?=(?:[^']*'[^']*')*[^']*$)");

            // Logs
            System.Console.WriteLine ($"split size: {vals.Length}");
            if (vals.Length < requiredSize)
                throw new ArgumentOutOfRangeException ($"ERR: Wrong split size. Required size {requiredSize}: {vals.Length}");

            for (int i = 0; i < vals.Length; i++)
                vals[i] = vals[i].Replace ("\'", "");

            return vals;
        }
    }
}