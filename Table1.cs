using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Wordprocessing;

namespace xls2text.T1
{
    public class Table1
    {
        public delegate void ParsedRow (Table1Model model);

        public static Table GetTable (Table1Model model)
        {
            TableRow tr = new TableRow ();
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_Project, isBold : true));
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Head.Project, isMerge : true));
            tr.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_Version, isBold : true));
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Head.Version, isMerge : true));
            tr.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr0 = new TableRow ();
            tr0.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_PrepBy, isBold : true));
            tr0.AppendChild (TableUtils.TableCellWithProperty (model.Head.Prep_by, isMerge : true));
            tr0.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr0.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_PrepOn, isBold : true));
            tr0.AppendChild (TableUtils.TableCellWithProperty (model.Head.Prep_on, isMerge : true));
            tr0.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr1 = new TableRow ();
            tr1.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_Module, isBold : true));
            tr1.AppendChild (TableUtils.TableCellWithProperty (model.Head.Test_Module, isMerge : true));
            tr1.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr1.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr1.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr1.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr2 = new TableRow ();
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestNo, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestName, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestDescription, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestPurpose, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_ExpectedResult, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestStatus, isBold : true));

            var table = TableUtils.GetTable ();

            table.AppendChild (tr);
            table.AppendChild (tr0);
            table.AppendChild (tr1);
            table.AppendChild (tr2);

            foreach (var row in model.Rows)
            {
                TableRow tr3 = new TableRow ();
                tr3.AppendChild (TableUtils.TableCellWithProperty (row.Test_No));
                tr3.AppendChild (TableUtils.TableCellWithProperty (row.Test_Name));
                tr3.AppendChild (TableUtils.TableCellWithProperty (row.Test_Description));
                tr3.AppendChild (TableUtils.TableCellWithProperty (row.Test_Purpose));
                tr3.AppendChild (TableUtils.TableCellWithProperty (row.Test_ExpectedResult));
                tr3.AppendChild (TableUtils.TableCellWithProperty (row.Test_Status));

                table.AppendChild (tr3);
            }

            return table;
        }

        public void ParseCSV (string path, ParsedRow delegateParsed)
        {
            using (var sr = new System.IO.StreamReader (path, encoding : System.Text.Encoding.GetEncoding ("utf-16")))
            {
                bool isFirst = true;

                TitleModel titleModel = null;
                TableHead lastHead = null;

                Table1Model tModel = new Table1Model ();

                string line;
                string currentTitle = null;
                while ((line = sr.ReadLine ()) != null)
                {
                    try
                    {
                        var vals = Utils.ParseCsvLine (line, 12);

                        if (isFirst)
                        {
                            titleModel = new TitleModel ()
                            {
                                Title_TestNo = vals[0],
                                Title_TestName = vals[2],
                                Title_TestDescription = vals[3],
                                Title_Module = vals[4],
                                Title_TestPurpose = vals[5],
                                Title_ExpectedResult = vals[6],
                                Title_TestStatus = vals[7],
                                Title_Project = vals[8],
                                Title_Version = vals[9],
                                Title_PrepBy = vals[10],
                                Title_PrepOn = vals[11],
                            };
                            isFirst = false;
                            continue;
                        }

                        string mergeTitle = vals[1].ToLower ();
                        if (currentTitle == null || currentTitle.Equals (mergeTitle))
                        {
                            currentTitle = mergeTitle;

                            TableHead head;
                            TestRow row;
                            GetHeadAndRow (vals, out head, out row);

                            lastHead = head;

                            tModel.Rows.Add (row);
                        }
                        else if (!currentTitle.Equals (mergeTitle))
                        {
                            tModel.Title = titleModel;
                            tModel.Head = lastHead;
                            delegateParsed?.Invoke (tModel);

                            // Set new Case
                            currentTitle = mergeTitle;

                            // Clean last rows
                            tModel.Rows.Clear ();

                            TableHead head;
                            TestRow row;
                            GetHeadAndRow (vals, out head, out row);

                            lastHead = head;
                            tModel.Head = head;
                            tModel.Rows.Add (row);
                        }
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine (e);
                    }
                }

                if (tModel.Rows.Count > 0)
                {
                    tModel.Title = titleModel;
                    tModel.Head = lastHead;
                    delegateParsed?.Invoke (tModel);
                }
            }
        }

        private void GetHeadAndRow (string[] vals, out TableHead head, out TestRow row)
        {
            head = new TableHead ()
            {
                Test_Module = vals[4],
                Project = vals[8],
                Version = vals[9],
                Prep_by = vals[10],
                Prep_on = vals[11],
            };

            row = new TestRow ()
            {
                Test_No = vals[0],
                Test_Name = vals[2],
                Test_Description = vals[3],
                Test_Purpose = vals[5],
                Test_ExpectedResult = vals[6],
                Test_Status = vals[7],
            };
        }
    }

    public class Table1Model : ITableModel
    {
        public TitleModel Title { get; set; }
        public TableHead Head { get; set; }
        public List<TestRow> Rows { get; set; } = new List<TestRow> ();
    }

    public class TableHead
    {
        public string Project { get; set; }
        public string Version { get; set; }
        public string Prep_by { get; set; }
        public string Prep_on { get; set; }
        public string Test_Module { get; set; }
    }

    public class TestRow
    {
        public string Test_No { get; set; }
        public string Test_Name { get; set; }
        public string Test_Description { get; set; }
        public string Test_Purpose { get; set; }
        public string Test_ExpectedResult { get; set; }
        public string Test_Status { get; set; }
    }

    public class TitleModel
    {
        public string Title_TestNo { get; set; }
        public string Title_TestName { get; set; }
        public string Title_TestDescription { get; set; }
        public string Title_Module { get; set; }
        public string Title_TestPurpose { get; set; }
        public string Title_ExpectedResult { get; set; }
        public string Title_TestStatus { get; set; }

        public string Title_Project { get; set; }
        public string Title_Version { get; set; }
        public string Title_PrepBy { get; set; }
        public string Title_PrepOn { get; set; }
    }
}