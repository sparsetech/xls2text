using System;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using static xls2text.TableUtils;

namespace xls2text
{
    interface ITable { }

    interface ITable<T> : ITable
    {
        public Table GetTable (T v);
    }

    interface ITableModel
    {

    }

    public static class TableUtils
    {
        public static TableCell TableCellWithProperty (string txt, bool isBold = false, int size = 22, bool isMerge = false, MergedCellValues merge = MergedCellValues.Restart)
        {
            TableCell tc1 = new TableCell ();
            tc1.TableCellProperties = new TableCellProperties (new TableCellWidth () { Type = TableWidthUnitValues.Pct, Width = "100" });

            if (isMerge)
                tc1.TableCellProperties.HorizontalMerge = new HorizontalMerge { Val = merge };

            RunProperties runPro = new RunProperties ();

            RunFonts runFont = new RunFonts () { Ascii = "Times New Roman", HighAnsi = "Times New Roman" };
            runPro.Append (runFont);

            FontSize fontSize = new FontSize () { Val = new StringValue ($"{size}") };
            runPro.Append (fontSize);

            if (isBold)
            {
                Bold bold = new Bold ();
                runPro.Append (bold);
            }

            var p = new Paragraph ();
            var r = new Run ();
            var t = new Text ($"{txt}");

            r.Append (runPro);
            r.Append (t);
            p.Append (r);
            tc1.Append (p);

            return tc1;
        }

        public static Table GetTable ()
        {
            Table table = new Table ();

            TableProperties tableProp = new TableProperties ();
            TableStyle tableStyle = new TableStyle () { Val = "TableGrid" };
            TableWidth tableWidth = new TableWidth () { Width = "5000", Type = TableWidthUnitValues.Pct };

            TableBorders property = new TableBorders (
                new TopBorder () { Val = new EnumValue<BorderValues> (BorderValues.BasicThinLines), Size = 8 },
                new BottomBorder () { Val = new EnumValue<BorderValues> (BorderValues.BasicThinLines), Size = 8 },
                new LeftBorder () { Val = new EnumValue<BorderValues> (BorderValues.BasicThinLines), Size = 8 },
                new RightBorder () { Val = new EnumValue<BorderValues> (BorderValues.BasicThinLines), Size = 8 },
                new InsideHorizontalBorder () { Val = new EnumValue<BorderValues> (BorderValues.BasicThinLines), Size = 8 },
                new InsideVerticalBorder () { Val = new EnumValue<BorderValues> (BorderValues.BasicThinLines), Size = 8 }
            );
            tableProp.Append (tableStyle, tableWidth, property);

            TableGrid tg = new TableGrid (
                new GridColumn () { Width = "1400" },
                new GridColumn () { Width = "1100" },
                new GridColumn () { Width = "2400" },
                new GridColumn () { Width = "1500" },
                new GridColumn () { Width = "1100" },
                new GridColumn () { Width = "1000" });
            tg.AppendChild (tableProp);
            table.AppendChild (tg);

            return table;
        }
    }
}