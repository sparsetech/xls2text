using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Wordprocessing;

namespace xls2text.T2
{
    public class Table2
    {
        public delegate void ParsedRow (Table2Model model);

        public static Table GetTable (Table2Model model)
        {
            TableRow tr = new TableRow ();
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_Project, isBold : true));
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Head.Project, isMerge : true));
            tr.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_Version, isBold : true));
            tr.AppendChild (TableUtils.TableCellWithProperty (model.Head.Version, isMerge : true));
            tr.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr1 = new TableRow ();
            tr1.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_Class, isBold : true));
            tr1.AppendChild (TableUtils.TableCellWithProperty (model.Head.Class, isMerge : true));
            tr1.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr1.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_PrepBy, isBold : true));
            tr1.AppendChild (TableUtils.TableCellWithProperty (model.Head.Prep_by, isMerge : true));
            tr1.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr2 = new TableRow ();
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_ClassDescription, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Head.ClassDescription, isMerge : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_PrepOn, isBold : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty (model.Head.Prep_on, isMerge : true));
            tr2.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr3 = new TableRow ();
            tr3.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_FunctionModule, isBold : true));
            tr3.AppendChild (TableUtils.TableCellWithProperty (model.Head.FunctionModule, isMerge : true));
            tr3.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr3.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr3.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr3.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr4 = new TableRow ();
            tr4.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_FunctionDescription, isBold : true));
            tr4.AppendChild (TableUtils.TableCellWithProperty (model.Head.FunctionDescription, isMerge : true));
            tr4.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr4.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr4.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));
            tr4.AppendChild (TableUtils.TableCellWithProperty ("", isMerge : true, merge : MergedCellValues.Continue));

            TableRow tr5 = new TableRow ();
            tr5.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestNo, isBold : true));
            tr5.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestName, isBold : true));
            tr5.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestDescription, isBold : true));
            tr5.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestPurpose, isBold : true));
            tr5.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_ExpectedResult, isBold : true));
            tr5.AppendChild (TableUtils.TableCellWithProperty (model.Title.Title_TestStatus, isBold : true));

            var table = TableUtils.GetTable ();

            table.AppendChild (tr);
            table.AppendChild (tr1);
            table.AppendChild (tr2);
            table.AppendChild (tr3);
            table.AppendChild (tr4);
            table.AppendChild (tr5);

            foreach (var row in model.Rows)
            {
                TableRow tr6 = new TableRow ();
                tr6.AppendChild (TableUtils.TableCellWithProperty (row.Test_No));
                tr6.AppendChild (TableUtils.TableCellWithProperty (row.Test_Name));
                tr6.AppendChild (TableUtils.TableCellWithProperty (row.Test_Description));
                tr6.AppendChild (TableUtils.TableCellWithProperty (row.Test_Purpose));
                tr6.AppendChild (TableUtils.TableCellWithProperty (row.Test_ExpectedResult));
                tr6.AppendChild (TableUtils.TableCellWithProperty (row.Test_Status));

                table.AppendChild (tr6);
            }

            return table;
        }

        public void ParseCSV (string path, ParsedRow delegateParsed)
        {
            using (var sr = new System.IO.StreamReader (path, encoding : System.Text.Encoding.GetEncoding ("utf-16")))
            {
                bool isFirst = true;

                TitleModel titleModel = null;
                TableHead lastHead = null;

                Table2Model tModel = new Table2Model ();

                string line;
                string currentTitle = null;
                while ((line = sr.ReadLine ()) != null)
                {
                    try
                    {
                        var vals = Utils.ParseCsvLine (line, 14);

                        if (isFirst)
                        {
                            titleModel = new TitleModel ()
                            {
                                Title_Class = vals[0],
                                Title_ClassDescription = vals[1],
                                Title_FunctionModule = vals[2],
                                Title_FunctionDescription = vals[3],
                                Title_TestNo = vals[4],
                                Title_TestName = vals[5],
                                Title_TestDescription = vals[6],
                                Title_TestPurpose = vals[7],
                                Title_ExpectedResult = vals[8],
                                Title_TestStatus = vals[9],
                                Title_Project = vals[10],
                                Title_Version = vals[11],
                                Title_PrepBy = vals[12],
                                Title_PrepOn = vals[13],
                            };
                            isFirst = false;
                            continue;
                        }

                        string mergeTitle = vals[2].ToLower ();
                        if (currentTitle == null || currentTitle.Equals (mergeTitle))
                        {
                            currentTitle = mergeTitle;

                            TableHead head;
                            TestRow row;
                            GetHeadAndRow (vals, out head, out row);

                            lastHead = head;

                            tModel.Rows.Add (row);
                        }
                        else if (!currentTitle.Equals (mergeTitle))
                        {
                            tModel.Title = titleModel;
                            tModel.Head = lastHead;
                            delegateParsed?.Invoke (tModel);

                            // Set new Case
                            currentTitle = mergeTitle;

                            // Clean last rows
                            tModel.Rows.Clear ();

                            TableHead head;
                            TestRow row;
                            GetHeadAndRow (vals, out head, out row);

                            lastHead = head;
                            tModel.Head = head;
                            tModel.Rows.Add (row);
                        }
                    }
                    catch (Exception e)
                    {
                        System.Console.WriteLine (e);
                    }
                }

                if (tModel.Rows.Count > 0)
                {
                    tModel.Title = titleModel;
                    tModel.Head = lastHead;
                    delegateParsed?.Invoke (tModel);
                }
            }
        }

        private void GetHeadAndRow (string[] vals, out TableHead head, out TestRow row)
        {
            head = new TableHead ()
            {
                Class = vals[0],
                ClassDescription = vals[1],
                FunctionModule = vals[2],
                FunctionDescription = vals[3],
                Project = vals[10],
                Version = vals[11],
                Prep_by = vals[12],
                Prep_on = vals[13],
            };

            row = new TestRow ()
            {
                Test_No = vals[4],
                Test_Name = vals[5],
                Test_Description = vals[6],
                Test_Purpose = vals[7],
                Test_ExpectedResult = vals[8],
                Test_Status = vals[9],
            };
        }
    }

    public class Table2Model : ITableModel
    {
        public TitleModel Title { get; set; }
        public TableHead Head { get; set; }
        public List<TestRow> Rows { get; set; } = new List<TestRow> ();
    }

    public class TableHead
    {
        public string Class { get; set; }
        public string ClassDescription { get; set; }
        public string FunctionModule { get; set; }
        public string FunctionDescription { get; set; }
        public string Project { get; set; }
        public string Version { get; set; }
        public string Prep_by { get; set; }
        public string Prep_on { get; set; }
    }

    public class TestRow
    {
        public string Test_No { get; set; }
        public string Test_Name { get; set; }
        public string Test_Description { get; set; }
        public string Test_Purpose { get; set; }
        public string Test_ExpectedResult { get; set; }
        public string Test_Status { get; set; }
    }

    public class TitleModel
    {
        public string Title_Class { get; set; }
        public string Title_ClassDescription { get; set; }
        public string Title_FunctionModule { get; set; }
        public string Title_FunctionDescription { get; set; }
        public string Title_TestNo { get; set; }
        public string Title_TestName { get; set; }
        public string Title_TestDescription { get; set; }
        public string Title_TestPurpose { get; set; }
        public string Title_ExpectedResult { get; set; }
        public string Title_TestStatus { get; set; }
        public string Title_Project { get; set; }
        public string Title_Version { get; set; }
        public string Title_PrepBy { get; set; }
        public string Title_PrepOn { get; set; }
    }
}